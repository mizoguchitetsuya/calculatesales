package jp.alhinc.mizoguchi_tetsuya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class CalculateSales {
	public static void main(String[] args) {

		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchAmounts = new HashMap<>();

		// 支店定義ファイル読み込み
		try {
			BufferedReader br = null;

			File branchLst = new File(args[0], "branch.lst");
			if (!branchLst.exists()) {
				System.out.print("支店定義ファイルが存在しません");
				return;
			}

			try {
				br = new BufferedReader(new FileReader(branchLst));

				String line;
				while ((line = br.readLine()) != null) {
					String[] code = line.split(",", -1);

					if (!code[0].matches("\\d{3}") || code.length != 2) {
						System.out.print("支店定義ファイルのフォーマットが不正です");
						return;
					}

					branchNames.put(code[0], code[1]);
					branchAmounts.put(code[0], 0L);
				}

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}

			// 集計
			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File dir, String filename) {
					File file = new File(dir, filename);
					return file.isFile() && filename.matches("\\d{8}\\.rcd");
				}
			};

			File[] rcdFiles = new File(args[0]).listFiles(filter);
			Arrays.sort(rcdFiles);
			for (int i = 0; i < rcdFiles.length - 1; ++i) {

				String rcdName = rcdFiles[i].getName();
				String nextRcdName = rcdFiles[i + 1].getName();
				String[] rcdSplitName = rcdName.split("\\.");
				String[] nextRcdSplitName = nextRcdName.split("\\.");

				int currentNumber = Integer.parseInt(rcdSplitName[0]);
				int nextNumber = Integer.parseInt(nextRcdSplitName[0]);

				if (nextNumber - currentNumber != 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}

			try {
				for (int i = 0; i < rcdFiles.length; i++) {
					br = new BufferedReader(new FileReader(rcdFiles[i]));

					String line1 = br.readLine();
					String line2 = br.readLine();
					String line3 = br.readLine();

					if (line3 != null) {
						System.out.print(rcdFiles[i] + "のフォーマットが不正です");
						return;
					}

					long amount = Long.parseLong(line2);

					if (!branchNames.containsKey(line1)) {
						System.out.println(rcdFiles[i] + "の支店コードが不正です");
						return;
					}
					branchAmounts.put(line1, branchAmounts.get(line1) + amount);

					String num = Long.toString(Math.abs(branchAmounts.get(line1)));
					if (num.length() > 10) {
						System.out.println("売上金額が10桁を超えました");
						return;
					}
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				br.close();
			}

			// 集計結果出力
			BufferedWriter bw = null;
			try {
				File branchOut = new File(args[0], "branch.out");
				bw = new BufferedWriter(new FileWriter(branchOut));

				for (Entry<String, String> entry : branchNames.entrySet()) {
					bw.write(entry.getKey() + "," + entry.getValue() + "," + branchAmounts.get(entry.getKey()));
					bw.newLine();
				}
				bw.close();

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				bw.close();
			}
		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}
}